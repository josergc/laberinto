/**
	Proyecto:
	Libro interactivo
	
	Hist�rico de versiones:
	1.0 - 6 de noviembre de 2006
	
	Descripci�n de la clase:
	Datos sobre un temporizador
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

public class Temporizador {
	public int nSegundos;
	public int segundosQueQuedan;
	public String salto;
	public boolean valido = true;
	
	public Temporizador(int nSegundos, String salto) {
		segundosQueQuedan = this.nSegundos = nSegundos;
		this.salto = salto;
	}
	
	public void reinicia() {
		segundosQueQuedan = nSegundos;
	}
	
	public boolean decrementa() {
		if (valido) {
			if (segundosQueQuedan > 0) {
				segundosQueQuedan--; 
				return true;
			}
		} else
			return true;
		return false;
	}
	
	public boolean finalizado() {
		return segundosQueQuedan == 0;
	}
}