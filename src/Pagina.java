/**
	Proyecto:
	Libro interactivo
	
	Hist�rico de versiones:
	1.0 - 6 de noviembre de 2006
	
	Descripci�n de la clase:
	Carga una p�gina
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

import java.io.*;
import java.util.*;
import javax.microedition.lcdui.*;

public class Pagina extends Form implements Runnable {
	protected boolean ultimaPagina = false;
	protected String nombrePagina;
	protected AvisoPaginaCargada apc;
	protected Vector melodias = new Vector();
	protected Vector opciones = new Vector();
	protected Vector temporizadoresLocales = new Vector();
	protected Vector iniciarTemporizadoresGlobales = new Vector();
	protected Vector finalizarTemporizadoresGlobales = new Vector();
	protected ChoiceGroup opcionesPagina = new ChoiceGroup("Elige",ChoiceGroup.EXCLUSIVE);
	
	public Pagina(String nombrePagina, AvisoPaginaCargada apc) {
		super(null);
		this.nombrePagina = nombrePagina;
		this.apc = apc;
		new Thread(this).start();
	}
	
	public void run() {
		DataInputStream dis = new DataInputStream(
			getClass().getResourceAsStream("/" + nombrePagina)
			);
		
		int cmd;
		try {
			// Interpreta el contenido del recurso
			while((cmd = dis.read()) != -1) {
				switch(cmd) {
					case Comandos.PARRAFO:
						append(new StringItem(" ",dis.readUTF()));
					break;
					case Comandos.OPCION:
						opciones.addElement(new Opcion(
							dis.readUTF(), // Descripci�n
							dis.readUTF()  // Salto
							));
					break;
					case Comandos.IMAGEN:
						String nombre = dis.readUTF();
						ImageItem ii = new ImageItem(
							null,
							null,
							ImageItem.LAYOUT_CENTER,
							null
							);
						append(ii);
						new Imagen(nombre,ii);
					break;
					case Comandos.INICIA_TEMPORIZADOR_GLOBAL:
						iniciarTemporizadoresGlobales.addElement(
							new TemporizadorGlobal(
								dis.readUTF(),     // Descripci�n
								dis.readInt(), // N� segundos
								dis.readUTF()      // Salto
								)
							);
					break;
					case Comandos.FINALIZA_TEMPORIZADOR_GLOBAL:
						finalizarTemporizadoresGlobales.addElement(
							new TemporizadorGlobal(
								dis.readUTF(), // Descripci�n
								0,             // N� segundos
								null           // Salto
								)
							);
					break;
					case Comandos.INICIA_TEMPORIZADOR_LOCAL:
						temporizadoresLocales.addElement(
							new Temporizador(
								dis.readInt(), // N� segundos
								dis.readUTF()      // Salto
								)
							);
					break;
					case Comandos.MUSICA:
						melodias.addElement(new Musica(
							dis.readUTF() // Fichero
							));
					break;
					case Comandos.SONIDO:
					break;
				}
			}
			
			// Comprueba si es la �ltima p�gina
			ultimaPagina = opciones.size() == 0;
			if (!ultimaPagina) {
				// Agrega las opciones al final de la p�gina
				append(opcionesPagina);
				Opcion o;
				Enumeration e = opciones.elements();
				while (e.hasMoreElements()) {
					o = (Opcion)e.nextElement();
					opcionesPagina.append(o.descripcion,null);
				}
			}
		} catch(IOException e) {
			append(new StringItem("Error",e.toString()));
		}

		// Avisa de la finalizaci�n de la carga
		if (apc != null) 
			apc.cargada(this);
	}
	
	public String saltar() {
		return ((Opcion)opciones.elementAt(opcionesPagina.getSelectedIndex())).salto;
	}
	
	public void hacerVisible(Display d) {
		d.setCurrent(this);
		// Inicia todos los temporizadores locales y las melod�as
	}
	
	public boolean esLaUltimaPagina() {
		return ultimaPagina;
	}
	
	public Enumeration devIniciarTemporizadoresGlobales() {
		return iniciarTemporizadoresGlobales.elements();
	}
	
	public Enumeration devFinalizarTemporizadoresGlobales() {
		return finalizarTemporizadoresGlobales.elements();
	}
	
	public String decTemporizadoresLocales() {
		Temporizador t;
		Enumeration e = temporizadoresLocales.elements();
		while (e.hasMoreElements()) {
			t = (Temporizador)e.nextElement();
			if (!t.decrementa()) {
				return t.salto;
			}
		}
		return null;
	}
	
	public boolean hayTemporizadoresLocales() {
		return temporizadoresLocales.size() > 0;
	}
}