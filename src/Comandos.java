public class Comandos {
	public static final byte 
		PARRAFO = 0,
		IMAGEN = 1,
		MUSICA = 2,
		SONIDO = 3,
		INICIA_TEMPORIZADOR_GLOBAL = 4,
		FINALIZA_TEMPORIZADOR_GLOBAL = 5,
		INICIA_TEMPORIZADOR_LOCAL = 6,
		OPCION = 7;
}