/**
	Proyecto:
	Libro interactivo
	
	Hist�rico de versiones:
	1.0 - 6 de noviembre de 2006
	
	Descripci�n de la clase:
	Esta interfaz es para avisar de cu�ndo una p�gina ha sido toltamente cargada
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

public interface AvisoPaginaCargada {
	public void cargada(Pagina p);
}