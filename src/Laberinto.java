/**
	Proyecto:
	Laberinto
	
	Hist�rico de versiones:
	1.0 - 8 de noviembre de 2006
	
	Descripci�n de la clase:
	Clase principal
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

public class Laberinto extends MIDlet implements AvisoLibroCerrado {
	protected void startApp() throws 
		MIDletStateChangeException
		{
		Display d = Display.getDisplay(this);
		new Libro("inicio",d,this);
	}
	protected void pauseApp() {
	}
	protected void destroyApp(boolean unconditional) throws 
		MIDletStateChangeException
		{
	}
	public void cerrado(Libro l) {
		try { destroyApp(false); } catch(MIDletStateChangeException e) {}
		notifyDestroyed();
	}
}