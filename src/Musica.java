/**
	Proyecto:
	Libro interactivo
	
	Hist�rico de versiones:
	1.0 - 6 de noviembre de 2006
	
	Descripci�n de la clase:
	Carga una melod�a y la pone en marcha en cuanto est� cargada
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

import java.io.*;
import javax.microedition.media.*;

public class Musica implements Runnable {
	protected String nombreFichero;
	public Player p = null;
	public Musica(String nombreFichero) {
		this.nombreFichero = nombreFichero;
		new Thread(this).start();
	}
	public void run() {
		try {
			p = Manager.createPlayer(
				getClass().getResourceAsStream("/" + nombreFichero),
				"audio/midi"
				);
		} catch(MediaException pe) {
		} catch(IOException e) {
		}
	}
}