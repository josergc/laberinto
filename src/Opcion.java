/**
	Proyecto:
	Libro interactivo
	
	Hist�rico de versiones:
	1.0 - 6 de noviembre de 2006
	
	Descripci�n de la clase:
	Datos sobre una opci�n
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

public class Opcion {
	public String descripcion;
	public String salto;
	
	public Opcion(String descripcion, String salto) {
		this.descripcion = descripcion;
		this.salto = salto;
	}
}