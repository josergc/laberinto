/**
	Proyecto:
	Libro interactivo
	
	Hist�rico de versiones:
	1.0 - 6 de noviembre de 2006
	
	Descripci�n de la clase:
	Datos sobre un temporizador
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

public class TemporizadorGlobal extends Temporizador {
	public String descripcion;
	
	public TemporizadorGlobal(String descripcion, int nSegundos, String salto) {
		super(nSegundos,salto);
		this.descripcion = descripcion;
	}
}