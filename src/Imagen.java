/**
	Proyecto:
	Libro interactivo
	
	Hist�rico de versiones:
	1.0 - 6 de noviembre de 2006
	
	Descripci�n de la clase:
	Carga una imagen y la asigna a un elemento imagen de un formulario
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

import java.io.*;
import javax.microedition.lcdui.*;

public class Imagen implements Runnable {
	String nombreFichero;
	ImageItem imagen;
	public Imagen(String nombreFichero, ImageItem imagen) {
		this.nombreFichero = nombreFichero;
		this.imagen = imagen;
		new Thread(this).start();
	}
	public void run() {
		try {
			imagen.setImage(Image.createImage("/" + nombreFichero + ".png"));
		} catch(IOException e) {
		}
	}
}