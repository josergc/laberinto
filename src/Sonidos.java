public class Sonidos {
	public static final byte
		d = 8,
		C4 = 60,
		A4 = (byte)(C4 - 4),
		B4 = (byte)(C4 - 2),
		D4 = (byte)(C4 + 2),
		E4 = (byte)(C4 + 4),
		F4 = (byte)(C4 + 6),
		G4 = (byte)(C4 + 7),
		SILENCIO = -1,
		VERSION = -2,
		TIEMPO = -3,
		RESOLUCION = -4,
		INICIO_BLOQUE = -5,
		FIN_BLOQUE = -6,
		TOCAR_BLOQUE = -7,
		PONER_VOLUMEN = -8,
		REPETIR = -9
		;
}