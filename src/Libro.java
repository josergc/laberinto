/**
	Proyecto:
	Libro interactivo
	
	Hist�rico de versiones:
	1.0 - 6 de noviembre de 2006
	
	Descripci�n de la clase:
	Gestiona el libro
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

import java.util.*;
import javax.microedition.lcdui.*;

public class Libro implements AvisoPaginaCargada, Runnable, CommandListener, AvisoRetornoDesdeDonativos {
	protected static final Command cmdElegir = new Command("Aceptar",Command.CANCEL,1);
	protected static final Command cmdCerrar = new Command("Cerrar",Command.OK,1);
	protected static final Command cmdSalir = new Command("Salir",Command.EXIT,1);
	protected static final Command cmdHacerDonativo = new Command("Hacer donativo",Command.OK,1);
	protected static final Command cmdAcercaDe = new Command("Acerca de...",Command.OK,1);

	protected AvisoLibroCerrado alc;
	
	public Hashtable temporizadoresGlobales = new Hashtable();
	public Display display;
	public Pagina paginaActual = null;
	public boolean paginaNoPasada = true;
	
	public Libro(String inicio, Display display, AvisoLibroCerrado alc) {
		this.display = display;
		this.alc = alc;
		
		Pagina p = new Pagina(inicio,this);
		p.addCommand(cmdHacerDonativo);
		p.addCommand(cmdAcercaDe);
	}
	
	public void cargada(Pagina p) {
		// Detiene los temporizadores
		paginaNoPasada = false;
	
		// Finaliza temporizadores globales 
		TemporizadorGlobal tg;
		Enumeration e = p.devFinalizarTemporizadoresGlobales();
		while (e.hasMoreElements()) {
			tg = (TemporizadorGlobal)e.nextElement();
			temporizadoresGlobales.remove(tg.descripcion);
		}
		// Inicia temporizadores globales
		e = p.devIniciarTemporizadoresGlobales();
		while (e.hasMoreElements()) {
			tg = (TemporizadorGlobal)e.nextElement();
			temporizadoresGlobales.put(tg.descripcion,tg);
		}
		
		// Inserta los comandos que se pueden ejecutar
		if (!p.esLaUltimaPagina())
			p.addCommand(cmdElegir);
		p.addCommand(cmdCerrar);
		p.setCommandListener(this);
		
		// Pone la p�gina en pantalla
		display.setCurrent(paginaActual = p);
		paginaNoPasada = true;
		new Thread(this).start();
	}
	
	public void run() {
		String salto = null;
		Enumeration etg;
		TemporizadorGlobal tg;
		while (paginaNoPasada && (temporizadoresGlobales.size() > 0 || paginaActual.hayTemporizadoresLocales())) {
		
			try { Thread.sleep(1000); } catch(InterruptedException e) {}
			
			// Ejecuta los temporizadores globales
			etg = temporizadoresGlobales.elements();
			while (etg.hasMoreElements()) {
				tg = (TemporizadorGlobal)etg.nextElement();
				if (tg.decrementa()) {
					new Pagina(tg.salto,this);
					return;
				}
			}
			
			// Ejecuta los temporizadores locales
			salto = paginaActual.decTemporizadoresLocales();
			if (salto != null) {
				new Pagina(salto,this);
				return;
			}
		}
		paginaNoPasada = true;
	}
	
	public void commandAction(Command c, Displayable d) {
		if (paginaNoPasada) {
			paginaNoPasada = false;
			if (c == cmdElegir) {
				// Pasar de p�gina
				new Pagina(paginaActual.saltar(),this);
			} else if (c == cmdCerrar) {
				// Cerrar libro
				PantallaMensaje pm = new PantallaMensaje(
					"Gracias por jugar",
					"Visita www.josergc.tk para conseguir m�s juegos.\nJos� Roberto Garc�a Chico\njosergc@lycos.es",
					null
					);
				pm.addCommand(cmdSalir);
				pm.setCommandListener(this);
				display.setCurrent(pm);
			} else if (c == cmdHacerDonativo) {
				new PantallaHacerDonativo(display,this);
			} else if (c == cmdAcercaDe) {
				PantallaMensaje pm = new PantallaMensaje(
					"Libro interactivo 1.0",
					"Esta aplicaci�n se distribuye con copyright bajos los t�rminos de la GNU General Public License (versi�n 2 o superior).\nPuedes encontrar una copia en http://www.gnu.org/licenses/gpl.html\nJos� Roberto Garc�a Chico\nwww.josergc.tk\njosergc@lycos.tk\n6 de noviembre de 2006",
					null
					);
				pm.addCommand(cmdCerrar);
				pm.setCommandListener(this);
				display.setCurrent(pm);
			} 
		} else if (c == cmdSalir) {
			if (alc != null)
				alc.cerrado(this);
		} else if (c == cmdCerrar) {
			display.setCurrent(paginaActual);
			paginaNoPasada = true;
			new Thread(this).start();
		}
	}
	
	public void retornoDesdeDonativos() {
		display.setCurrent(paginaActual);
		paginaNoPasada = true;
		new Thread(this).start();
	}

}