/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	En esta pantalla se env�a autom�ticamente el c�digo recibido para hacer 
	efectivo el micropago.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import java.io.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.*;
import javax.wireless.messaging.*;

public class PantallaEnviarCodigoMicropago extends Form implements Runnable {
	protected String codigoAEnviar;
	protected PantallaHacerDonativo phd;

	public PantallaEnviarCodigoMicropago(PantallaHacerDonativo phd, String codigoAEnviar) {
		super("Enviando c�digo...");
		
		this.phd = phd;
		this.codigoAEnviar = codigoAEnviar;
	}
	
	public void run() {
		// Env�a el mensaje
		synchronized (this) {
			HttpConnection conexion = null;
			InputStream is = null;
			try {
				StringItem si;
				append(si = new StringItem("Abriendo conexi�n...",null));
				conexion = (HttpConnection)Connector.open(
					"http://69.36.9.147:8090/clientes/SMS_API_OUT.jsp?cliente=SEQWR&codigo=" + codigoAEnviar
					);
				si.setText("�hecho!");
				append(si = new StringItem("Recibiendo respuesta...",null));
				int rc = conexion.getResponseCode();
				if (rc != HttpConnection.HTTP_OK) {
					phd.muestraError(
						"Error al enviar una consulta",
						"Recibido el c�digo HTTP " + rc + ".\nPuede volverlo a intentar m�s tarde, o bien contactar conmigo en josergc@lycos.es o bien visitar la web www.josergc.tk"
						);
				} else {
					si.setText("�hecho!");
					append(si = new StringItem("Obteniendo mensaje retorno...",null));
					is = conexion.openInputStream();
					si.setText("�hecho!");
					String cadenaRecibida = new String(Utiles.flujoByteArray(is));
					if (cadenaRecibida.length() == 0) {
						phd.muestraError("Error","Se ha obtenido una cadena vac�a.\nP�ngase en contacto conmigo por el e-mail josergc@lycos.es o por la p�gina web www.josergc.tk");
					} else
						switch(cadenaRecibida.charAt(13)) {
							case '1':
								phd.muestraMensaje("C�digo v�lido",cadenaRecibida);
							break;
							case '2':
								phd.muestraMensaje("C�digo v�lido pero usado",cadenaRecibida);
							break;
							case '3':
								phd.muestraMensaje("C�digo no v�lido, vu�lvalo a intentar",cadenaRecibida);
							break;
							case '4':
								phd.muestraMensaje("Error interno",cadenaRecibida + "\nP�ngase en contacto conmigo por el e-mail josergc@lycos.es o por la p�gina web www.josergc.tk");
							break;
							default:
								phd.muestraMensaje("Error desconocido '" + cadenaRecibida.charAt(13) + "'",cadenaRecibida + "\nP�ngase en contacto conmigo por el e-mail josergc@lycos.es o por la p�gina web www.josergc.tk");
						}
				}
			} catch(IOException e) {
				phd.muestraError(
					"Error de entrada/salida",
					"No se ha podido realizar la operaci�n.\n" + e.toString() + "\nP�ngase en contacto conmigo por el e-mail josergc@lycos.es o por la p�gina web www.josergc.tk"
					);
			} catch(ClassCastException e) {
				phd.muestraError(
					"No se puede realizar la acci�n",
					"Este dispositivo m�vil no est� capacitado para hacer conexiones HTTP.\nP�ngase en contacto conmigo por el e-mail josergc@lycos.es o por la p�gina web www.josergc.tk"
					);
			} finally {
				try { if (is != null) is.close(); } catch(IOException e) { }
				try { if (conexion != null) conexion.close(); } catch(IOException e) { }
			}
		}
	}
}