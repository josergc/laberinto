/**
	Proyecto:
	Comun
	
	Hist�rico de versiones:
	1.0 - 6 de noviembre de 2006
	
	Descripci�n de la clase:
	Esta interfaz es usada por la funci�n de donativos para avisar a otra 
	aplicaci�n de que se le ha devuelto el control.
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

public interface AvisoRetornoDesdeDonativos {
	public void retornoDesdeDonativos();
}